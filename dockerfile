# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy WAR into image
COPY target/buy-mileage.jar /buy-mileage.jar
# run application with this command line[
CMD ["java", "-jar", "/buy-mileage.jar"]
